# -*- coding: utf-8 -*

from setuptools import setup

setup(name='paquete3',
      version='1.0',
      description='Soy un paquete. No de verdad que lo soy',
      author='el paquete',
      author_email='hmarrao@ekergy.es',
      url='http://priceprofor.ekergy.es/',
      packages=['paquete3'],
      install_requires=['awesome-slugify','bottle','rauth','beaker','pymongo','setuptools',
                        'Jinja2>=2.7.2'],

     )
#Python: Guia rapido de estructura y uso


## Paso 0:

1. Se recomienda que instales un ambiente virtual para tu instalación de python [virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/).

2. Se recomienda que mejores tu interpretador de python añadiendo el [historico](http://rc98.net/pystartup) y el [auto completado](http://rc98.net/pystartup). 

3. Se recomienda indicar el encoding del fichero en la primera linea del fichero .py:
```
# -*- coding: utf-8 -*-
```

## Paso 1: script

Un simples script ```python``` [script.py](https://bitbucket.org/marrao/quickreferencepython/src/0a1f03ef96d9b2733185d1df5f9306a05ad2840e/script.py?at=master) 
ejecuta desde una terminal con:
```
python script.py
``` 

## Paso 2: modulo

un simples modulo ```python``` [modulo.py](https://bitbucket.org/marrao/quickreferencepython/src/0a1f03ef96d9b2733185d1df5f9306a05ad2840e/modulo.py?at=master)
ejecuta desde una terminal con:

```
python
```
y en la terminal de python:
```
>>> import modulo
>>> modulo.funcion()
>>> exit()
```

> al importar el modulo python se genera un compilado de python con la extención pyc.

## Paso 3: paquete

un modulo con parte de script [modulo2.py](https://bitbucket.org/marrao/quickreferencepython/src/0a1f03ef96d9b2733185d1df5f9306a05ad2840e/modulo2.py?at=master):
```
if __name__ == '__main__':
	... 
```
para ejecutarlo:
```
python modulo2.py
```

## Paso 4: paquete executable

un paquete python [paquete1](https://bitbucket.org/marrao/quickreferencepython/src/0a1f03ef96d9b2733185d1df5f9306a05ad2840e/paquete1/?at=master):

sea cual sea tu systema operativo un paquete python es una carpeta con el fichero ```__init__.py``` dentro.

un paquete puede contener modulos dentro como los definidos anteriormente. Si se ejecuta un paquete desde la linea de comandos obtenemos un error.
```
python paquete1
```
para hacer un paquete ejecutable hay que añadir el fichero ```__main__.py``` 

> el fichero ```__main__.py``` es la parte de script de un paquete como es la parte de
  ```if __name__ == '__main__': ```
en un modulo.

## Paso 5: paquete con modulos

un paquete python con modulos dentro [paquete2](https://bitbucket.org/marrao/quickreferencepython/src/0a1f03ef96d9b2733185d1df5f9306a05ad2840e/paquete2/?at=master):

se puede incluir modulos dentro de los paquetes y usarlos desde el main.

ademas desde el interpretador puedes importar el paquete y usar sus modulos funciones o variables.
```
>>> import paquete2
>>> paquete2.variable
'variable del paquete 2'
>>> from paquete2 import modulo3
>>> modulo3.funcion()
'funcion del modulo3'
```

## Paso 6: PYTHONPATH

El ```PYTHONPATH``` es la variable de entorno que python usa para localizar modulos y paquetes.

El ```PYTHONSTARTUP``` es la variable de entorno que python usa para localizar el script python de inicio de python (o sea siempre que se escribe la instrucion python la primera cosa que se ejecuta es lo que tenga en este script).



Puedes añadir una carpeta al pythonpath y usarlo desde cualquier punto del sistema operativo
```
export PYTHONPATH=$PYTHONPATH:$(realpath carpetanormal)
```
y desde cualquier punto de tu sistema operativo puedes llamar la modulo que este dentro de la ```carpetanormal```

```
python -m paquete3
```

tambien puedes manipular el ```PYTHONPATH``` desde python manipulando el ```sys.path```


```
>>> import sys
>>> sys.path
```

## Paso 7: setup.py

Mayor parte de las veces con manipular el PYTHONPATH es suficiente para 'instalar' un paquete o un modulo pero la forma standard de python para liberar software es usando el [setup.py](https://bitbucket.org/marrao/quickreferencepython/src/0a1f03ef96d9b2733185d1df5f9306a05ad2840e/carpetaconsoftware/setup.py?at=master)

el formato standard del fichero es:
```
# -*- coding: utf-8 -*

from setuptools import setup

setup(name='paquete3',
      version='1.0',
      description='Soy un paquete. No de verdad que lo soy',
      author='el paquete',
      author_email='hmarrao@ekergy.es',
      url='http://priceprofor.ekergy.es/',
      packages=['paquete3'],
      install_requires=['awesome-slugify','bottle','rauth','beaker','pymongo','setuptools',
                        'Jinja2>=2.7.2'],

     )
```

para usar este fichero lo puedes hacer de dos formas:

1. instalar le paquete en todo el sistema operativo (puede ser que sea necesario el sudo):
```
python setup.py install
```

y asi tiene el paquete listo para usar desde cualquier punto del sistem operativo con la instruccion:
```
python -m paquete3
```



